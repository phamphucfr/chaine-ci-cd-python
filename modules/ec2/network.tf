data "aws_subnet" "selected" {
    id = var.var_subnet_id
}

resource "aws_security_group" "jpy_sg_v2" {
  name        = var.var_sg
  vpc_id      = data.aws_subnet.selected.vpc_id

  ingress {
    description      = "apache2 http"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.var_sg
  }
}

resource "aws_security_group_rule" "ssh" {
    security_group_id = aws_security_group.jpy_sg_v2.id
    description = "ssh pour ansible" 
    type = "ingress"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "jenkins" {
    security_group_id = aws_security_group.jpy_sg_v2.id
    description = "port jenkins" 
    type = "ingress"
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
} 

resource "aws_security_group_rule" "netdata" {
    security_group_id = aws_security_group.jpy_sg_v2.id
    description = "port pour Netdata" 
    type = "ingress"
    from_port = 19999
    to_port = 19999
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
} 