resource "aws_key_pair" "jpy_ssh_v2" {
    key_name = var.var_ssh
    public_key = file("~/.ssh/id_rsa.pub")  # créer paire de clé SSH aws à partir de la clé publique générée dans le script
}

# créer d'abord la machine principale
resource "aws_instance" "my_ec2_principal" {
    depends_on = [
        aws_key_pair.jpy_ssh_v2,
        aws_security_group.jpy_sg_v2,
        aws_security_group_rule.ssh,
        aws_security_group_rule.jenkins
    ]   
    ami = var.var_principal_ami

    vpc_security_group_ids = [aws_security_group.jpy_sg_v2.id]
    key_name = aws_key_pair.jpy_ssh_v2.key_name

    instance_type = var.var_principal_type
    
    disable_api_termination= true

    subnet_id = var.var_subnet_id
    associate_public_ip_address = true

    # install Ansible sur la VM principale et définir un mot de passe par défaut pour user admin
    user_data = <<-EOF
        #!/bin/bash
        echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu focal main" | sudo tee /etc/apt/sources.list.d/ansible.list
        sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
        sudo apt update
        sudo apt install -y ansible

        sudo usermod --password $(openssl passwd -6 'jpy') admin
        EOF 

    tags = {
        Name = "jpy_principal_debian_v2"
    }
}

# créer ensuite 3 machine cicd/prod/test
resource "aws_instance" "my_ec2" {
    depends_on = [
        aws_key_pair.jpy_ssh_v2,
        aws_security_group.jpy_sg_v2,
        aws_security_group_rule.ssh,
        aws_security_group_rule.jenkins
    ]  

    for_each = var.var_ami  # on boucle sur la liste des images AMI
    ami = "${each.value}"

    vpc_security_group_ids = [aws_security_group.jpy_sg_v2.id]
    key_name = aws_key_pair.jpy_ssh_v2.key_name

    instance_type = var.var_instance_type
    
    disable_api_termination= true

    subnet_id = var.var_subnet_id
    associate_public_ip_address = true
    
    tags = {
        Name = each.key
    }

}

resource "local_file" "ec2_public_ip" {  
    depends_on = [
      aws_instance.my_ec2
    ]
    # ça boucle sur chaque resource my_ec2 créé en haut
    for_each = var.var_ami
    content = (each.key == "jpy_cicd_debian_v2") ? "[ec2cicd]\n ${aws_instance.my_ec2[each.key].public_ip} ansible_user=admin \n" : "[ec2prodtest]\n ${aws_instance.my_ec2[each.key].public_ip} ansible_user=admin \n"
    filename = "${each.key}"  # un fichier contenant IP publique par EC2 
}

resource "null_resource" "cicd_inv" {
    depends_on = [
      local_file.ec2_public_ip
    ]
    for_each = var.var_ami
    # ajouter IP publique selon son groupe à notre fichier inventory de Ansible
    provisioner "local-exec" {
      command = "cat ${each.key} >> cicd.inv"      
    }  
}

# Partie automatisation des playbooks sur la VM principale
resource "null_resource" "my_ec2_principal_playbooks" {
    depends_on = [
      null_resource.cicd_inv
    ]

    # Connexion vers la VM principale en SSH
    connection {
        type = "ssh"
        user = "admin"
        host = aws_instance.my_ec2_principal.public_ip
        port = 22
        private_key = file("~/.ssh/id_rsa")
    }

 # cette étape supplémentaire permet ajouter la connexion SSH à cicd/prod/test depuis VM principale
    provisioner "file" { 
        source = "~/.ssh/id_rsa"  # copie de clé privée vers .ssh de l'user admin sous VM principale
        destination = ".ssh/id_rsa"     
    }

    # création d'un repo de destination
    provisioner "remote-exec" {
        inline = [
            "mkdir ~/deploy_ansible" 
         ]
    }
   
    # le fichier inventory généré en local sera copié vers VM principale
    provisioner "file" {
        source = "cicd.inv"  # endroit actuel de terraform
        destination = "deploy_ansible/cicd.inv" # sous dossier de l'user admin 
    }

    # ce fichier config nous aide à bypass la première SSH connexion checking
    # cela est très utile pour l'automatisation des taches Ansible sur VM principale
    provisioner "file" {
        source = "config"  # endroit actuel de terraform
        destination = ".ssh/config" # sous dossier de l'user admin 
    }

    # Copie des playbooks de la partie Ansible vers machine distante
    provisioner "file" {
        source = "playbooks/"
        destination = "deploy_ansible/"
    }

    # modifier le droit 400 pour clé privé sinon VM principale ne peut pas se connecter aux autres
    provisioner "remote-exec" {
        inline = [
            "chmod 400 ~/.ssh/id_rsa",
            "chmod +x deploy_ansible/play.sh",
        ]     
    }

}

