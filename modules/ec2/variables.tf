variable "var_ami" {
    type = map
    description = "image server ami aws"
}

variable "var_principal_ami" {
    type = string
    description = "image pour la machine principale"
}

variable "var_instance_type" {
    type = string
    description = "type instance"
}

variable "var_principal_type" {
    type = string
    description = "type instance de la machine principale"
}

variable "var_subnet_id" {
    type = string
    description = "subnet id"
}

variable "var_sg" {
    type = string
    description = "groupe de sécu"
}

variable "var_ssh" {
  type        = string
  description = "nom ssh key"
}
