#!/bin/bash
# sudo apt update -y
# sudo apt upgrade -y
# sudo apt-get install -y gnupg software-properties-common curl
# curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
# sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

# apt update && apt install -y terraform

# terraform --version

# terraform -install-autocomplete

ssh-keygen -f ~/.ssh/id_rsa -q -N "" # ssh-keygen en mode very quiet
 
echo > cicd.inv  # ceci crée un fichier .inv pour ansible , si ça existe , le contenu sera réinit

terraform init --upgrade

terraform plan

terraform apply