FROM python:slim-bullseye
WORKDIR /pyapp
COPY . /pyapp/

RUN pip3 install -r requirements.txt

EXPOSE 80

ENTRYPOINT [ "flask" ]
CMD [ "run", "--host=0.0.0.0", "--port=5001"]



