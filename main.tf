module "ec2" {
  source = "./modules/ec2"
  var_ami = {
    "jpy_cicd_debian_v2" = "ami-00189fd46154b0f9d"
    "jpy_test_debian_v2" = "ami-00189fd46154b0f9d"
    "jpy_prod_debian_v2" = "ami-00189fd46154b0f9d"
    }
  var_principal_ami = "ami-00189fd46154b0f9d"
  var_ssh = "jpy_ssh_v2"
  var_principal_type = "t3.small"
  var_instance_type = "t3.micro"
  var_subnet_id = "subnet-059d912ceeedbefc0"
  var_sg = "jpy_sg_v2"
}